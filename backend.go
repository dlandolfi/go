package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func main() {
	// handlefunc (pattern str, handler func(responsew, *req))
	http.HandleFunc("/", index)
	http.HandleFunc("/test", test)
	fmt.Println("Listening on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", nil)) // nil handler => DefaultServeMux

}

func index(w http.ResponseWriter, r *http.Request) {
	// fprint vs writestring
	// fmt.Fprintf(w, "okayyyy")
	io.WriteString(w, "okayyyyyyyy")
}

func test(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "<h1>this is a test</h1>")
}
