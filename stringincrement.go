package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	s := "hell0"
	s1 := "hello1"
	fmt.Println(stringIncrement(s))
	fmt.Println(stringIncrement(s1))
}

func stringIncrement(s string) string {
	split := strings.Split(s, "")
	last := len(split) - 1

	// Atoi takes in a string and returns an error if it is not an int
	// var int is the int at the last index
	if int, err := strconv.Atoi(split[last]); err == nil {
		// add 1 and convert last index value back to string
		split[last] = strconv.Itoa(int + 1)
	} else {
		split = append(split, "1")
	}

	join := strings.Join(split, "")

	return join
}
