package main

import "fmt"

func main() {
	sum (1,2)
	sum (2,4,8)

	a := []int{}
	for i := 1; i <= 100; i++ {
		a = append(a, i)
	}

	// sum (a)
	// total := 0
	i := 0
	for _, total := range a {
		i = i + total
	}
	fmt.Println(i)

}

func sum(nums ...int){
	fmt.Print(nums, " ")
	i := 0
	for _, num := range nums {
		i = i + num
	}
	fmt.Println(i)
}